
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import collections
from progress.bar import Bar
import random

if __name__ == '__main__':


    #########task1: comparison of different network models (WS, BA, G(N,p), G(N,L))
    N = 1000

    # YOUR CODE HERE
    G = nx.Graph()
    G.add_edges_from([(1, 2), (1, 3)])
    title = 'sample graph'
    # YOUR CODE HERE

    # degree distribution
    degrees = collections.Counter(dict(G.degree).values())

    plt.figure()
    plt.vlines(list(degrees.keys()), 0, [v / N for v in degrees.values()], '#99FFFF')
    plt.plot(list(degrees.keys()), [v / N for v in degrees.values()], 'o')
    plt.title(title, fontsize=20)
    plt.xlabel("degree", fontsize=15)
    plt.ylabel("distribution", fontsize=15)
    plt.show()

    # average degree
    average_degree = sum([k * v for k, v in degrees.items()]) / sum(degrees.values())

    # cluster coefficients distribution
    clust_coefs = list(nx.algorithms.cluster.clustering(G).values())
    plt.figure()
    plt.hist(clust_coefs, density=True)
    plt.title(title, fontsize=20)
    plt.xlabel("clustering coefficient", fontsize=15)
    plt.ylabel("distribution", fontsize=15)
    plt.show()

    # average cluster coefficient
    av_clust_coef = np.mean(clust_coefs)
    # or the same result:
    av_clust_coef = nx.algorithms.cluster.average_clustering(G)


    # diameter
    diameter = nx.diameter(G)

    # average shortest path length
    average_path_length = nx.average_shortest_path_length(G)




######################## task 2 : checking the robustness of the network
    def check_robustness1(G, f_vec):
        # YOUR CODE HERE

    # function for method from task 2: removing nodes with highest degree
    def check_robustness2(G, f_vec):
        # calculate the giant cluster size of the original graph
        P0 = len(max(nx.connected_components(G), key=len, default=[]))
        # sort nodes with respect to their degrees. Nodes with equal degree are returned in random order
        sorted_nodes = list(zip(*sorted(G.degree, key=lambda x: x[1]+random.random(), reverse=True)))[0]
        # initialize relsize_vec to an empty list
        relsize_vec = []
        #for each fraction f given in the argument f_vec
        for f in f_vec:
            # make a copy of the original graph
            G_temp = nx.Graph(G)
            # calculate the number of nodes to remove from the fraction of nodes f
            f0 = int(f * len(G.nodes))
            # select the nodes to remove, first f0 elements from the list sorted_nodes
            nodes_to_remove = sorted_nodes[:f0]
            # remove the nodes from the copied graph
            G_temp.remove_nodes_from(nodes_to_remove)
            # calculate the giant cluster size of the graph with deleted nodes
            Pf = len(max(nx.connected_components(G_temp), key=len, default=[]))
            # store the relative giant cluster size in the list relsize_vec
            relsize_vec.append(Pf / P0)
        return relsize_vec
    # example usage: plt.plot([0, 0.5, 1], check_robustness2(G, [0, 0.5, 1]))

    # YOUR CODE HERE


    ########task 3 - calculations for WS model

    # YOUR CODE HERE


